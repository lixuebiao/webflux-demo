import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version Kotlin.version
    id("org.springframework.boot") version Spring.Boot.version
    id("io.spring.dependency-management") version Spring.Boot.spring_dependency_management_version
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin.plugin.spring/org.jetbrains.kotlin.plugin.spring.gradle.plugin
    kotlin("plugin.spring") version Kotlin.version
}

ext["lombok.version"] = Tools.lombok_version
ext["spring-security.version"] = Spring.Security.version
ext["kotlin.version"] = Kotlin.version

group = Weike.group
version = Weike.version

java.sourceCompatibility = Weike.javaVersion
java.targetCompatibility = Weike.javaVersion

repositories {
    maven(url = "https://maven.aliyun.com/nexus/content/groups/public/")
//    maven(url = "http://localhost:8081/repository/maven-public/")
    mavenCentral()
    mavenLocal()
}

//allprojects是根Project的一个属性
// 块用于添加根工程和所有子工程的配置
allprojects {

    repositories {
        maven(url = "https://maven.aliyun.com/nexus/content/groups/public/")
//        maven(url = "http://localhost:8081/repository/maven-public/")
        mavenCentral()
        mavenLocal()
    }

}

//subprojects和allprojects一样，
// 也是父Project的一个属性在父项目的build.gradle脚本里，但subprojects()方法用于配置所有的子Project（不包含根Project）
//用于配置子模块信息
subprojects {

    apply {
        plugin("java")
        plugin("kotlin")
    }

//    plugin(kotlin("jvm"))
//    group = Weike.Press.group

    repositories {
        maven(url = "https://maven.aliyun.com/nexus/content/groups/public/")
//        maven(url = "http://localhost:8081/repository/maven-public/")
        mavenCentral()
        mavenLocal()
    }

    configure<JavaPluginExtension> {
        sourceCompatibility = Weike.javaVersion
        targetCompatibility = Weike.javaVersion
    }

    dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(Logging.Slf4j.api)
        implementation(Logging.Slf4j.jcl_over_slf4j)
        implementation(Tools.hutool)
        compileOnly(Tools.lombok)
        annotationProcessor(Tools.lombok)
        testCompileOnly(Tools.lombok)
        testAnnotationProcessor(Tools.lombok)

        testImplementation(Junit.junit5_jupiter_api)
        testImplementation(Junit.junit5_jupiter)
        testRuntimeOnly(Junit.junit5_jupiter_engine)
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = Weike.jvmTarget
        }
    }
}
