import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
//    war
    id("org.springframework.boot") //version Spring.Boot.version
    id("io.spring.dependency-management") //version Spring.Boot.spring_dependency_management_version
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin.plugin.spring/org.jetbrains.kotlin.plugin.spring.gradle.plugin
    kotlin("plugin.spring") //version Kotlin.version
}

group = Weike.group
version = Weike.version

java.sourceCompatibility = Weike.javaVersion
java.targetCompatibility = Weike.javaVersion

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
//    implementation("org.springframework.boot:spring-boot-starter-data-redis")
    implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
//    implementation("org.springframework.boot:spring-boot-starter-data-redis-reactive")
    implementation("org.springframework.boot:spring-boot-starter-json")
//    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    runtimeOnly("mysql:mysql-connector-java")
    runtimeOnly("dev.miku:r2dbc-mysql")
    implementation("dev.miku:r2dbc-mysql")
    implementation("io.r2dbc:r2dbc-pool")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation(Dependencies.jsonp)
//    implementation(project(":weike-extension-modules:jwt"))
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("io.projectreactor:reactor-test")
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }

//    all {
//        exclude("org.springframework.boot", "spring-boot-starter-logging")
//    }
}

configure<JavaPluginExtension> {
    sourceCompatibility = Weike.javaVersion
    targetCompatibility = Weike.javaVersion
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = Weike.jvmTarget
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
