package com.weike.bsp.security.authentication.captcha

import org.springframework.security.core.AuthenticationException

/**
 * @author tiger
 * -- create at 2019-05-30 16:40
 */
class BadCaptchaException @JvmOverloads constructor(
        msg: String?,
        t: Throwable? = null) : AuthenticationException(msg, t) {

    companion object {
        private const val serialVersionUID = -3203631590493348337L
    }
}