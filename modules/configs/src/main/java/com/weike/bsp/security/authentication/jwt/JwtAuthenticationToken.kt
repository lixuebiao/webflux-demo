package com.weike.bsp.security.authentication.jwt

import com.fasterxml.jackson.annotation.JsonIgnore
import com.weike.bsp.model.AccessToken
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority

/**
 * jwt token 认证
 *
 * @see UsernamePasswordAuthenticationToken
 */
class JwtAuthenticationToken @JvmOverloads constructor(
        principal: Any?,
        credentials: Any?,
        authorities: Collection<GrantedAuthority?> = emptySet(),
        /**
         * 格式：Beare xxxxxxxxxxxxxxxxxx
         * 或者
         * @see AccessToken
         */
        var token: Any
) : UsernamePasswordAuthenticationToken(principal, credentials, authorities) {

    @get:JsonIgnore
    val accessToken: AccessToken?
        get() = if (token is AccessToken) {
            token as AccessToken
        } else null

    @JsonIgnore
    override fun getCredentials(): Any? {
        return super.getCredentials()
    }

    companion object {
        private const val serialVersionUID = 8351193389385539173L
    }
}
