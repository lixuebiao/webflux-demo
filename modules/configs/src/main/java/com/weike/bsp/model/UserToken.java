package com.weike.bsp.model;

import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import lombok.Data;

import java.util.function.BiFunction;

@Data
public class UserToken implements BiFunction<Row, RowMetadata, UserToken> {

    private AccessToken accessToken;

    private SimpleUser user;

    @Override
    public UserToken apply(Row row, RowMetadata rowMetadata) {
        return UserToken.convert(row, rowMetadata);
    }

    public static UserToken convert(Row row, RowMetadata rowMetadata) {
        UserToken ut = new UserToken();
        ut.accessToken = AccessToken.convert(row, rowMetadata);
        ut.user = SimpleUser.convert(row, rowMetadata);
        return ut;
    }
}
