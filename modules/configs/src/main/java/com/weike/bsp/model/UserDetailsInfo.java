package com.weike.bsp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.lang.NonNull;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author tiger
 * -- create at 2019/12/20 14:38
 */
@Setter
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class UserDetailsInfo implements UserDetails, CredentialsContainer {

    /**
     * 登录用户缓存 key
     */
    public static final String LOGIN_USER_DETAILS_KEY = "LOGIN_USER_DETAILS_KEY";

    private static final long serialVersionUID = 4201417277714550437L;

    private static final Log logger = LogFactory.getLog(UserDetailsInfo.class);


    @NonNull
    @Setter(AccessLevel.NONE)
    @EqualsAndHashCode.Include
    private final SimpleUser user;

    public UserDetailsInfo(@NonNull SimpleUser user) {
        this.user = user;
    }

    @Override
    public void eraseCredentials() {
        user.setPassword(null);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> all = new HashSet<>();

        return all;
    }

    @NonNull
    public SimpleUser getUser() {
        return user;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return user.isActivated();
    }

}
