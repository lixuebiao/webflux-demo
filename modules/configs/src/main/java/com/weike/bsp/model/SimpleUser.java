package com.weike.bsp.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.weike.bsp.common.util.Constant;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.util.function.BiFunction;

/**
 * @author lxb
 *
 * - 2019-02-15 update
 * - v1.0.0
 * - javadoc
 * 对应数据库 pub_user，此处的字段无论序列化还是反序列化，不允许忽略
 *
 * 如果返回数据时，需要忽略某些字段，那么用户自定义 VO
 */
@ToString(callSuper = true)
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class SimpleUser {

    /**
     * 登录次数 key
     */
    public static final String LOGIN_ERROR_TIMES = "LOGIN_ERROR_TIMES";

    /**
     * 默认登录次数
     */
    public static final int DEFAULT_LOGIN_TIMES = 8;
    private static final long serialVersionUID = -2292648391798868483L;

    /**
     * 用户编号(主键)
     */
    @EqualsAndHashCode.Include
    private Long userId;

    /**
     * 用户名（登录）
     */
    @EqualsAndHashCode.Include
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 真实姓名
     */
    private String realname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户密码
     */
    @Deprecated
    private String password;

    /**
     * 用户性别
     * 0,1,2
     * 未知/男/女
     */
    @PositiveOrZero
    private Integer gender;

    /**
     * 用户生日
     */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = Constant.DEFAULT_DATE_FORMAT, shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = Constant.DEFAULT_DATE_FORMAT, iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthday;

    /**
     * 用户所属机构
     */
    private Long orgId;

    /**
     * org name
     */
    private String orgName;

    /**
     *  用户职务/岗位 id
     */
    private Long postId;

    /**
     * 用户职务/岗位
     */
    private String duty;

    /**
     * 用户电话
     */
    private String telephone;

    /**
     * 邮箱
     */
    @Email
    private String email;

    /**
     * QQ
     */
    private String qq;

    /**
     * 微信
     */
    private String weixin;

    /**
     * 用户描述
     */
    private String description;

    /**
     * 是否可用(true:正常/false:禁用)
     * 对应数据库中的 is_enabled
     * <p>
     * 此处使用activated 代替 enabled
     */
    private boolean activated = true;

    private boolean accountNonExpired = true;

    private boolean accountNonLocked = true;

    private boolean credentialsNonExpired = true;


    public static SimpleUser convert(Row row, RowMetadata rowMetadata) {
        SimpleUser simpleUser = new SimpleUser();
        simpleUser.userId = row.get("user_id", Long.class);
        simpleUser.orgId = row.get("org_id", Long.class);
        simpleUser.username = row.get("username", String.class);
        simpleUser.nickname = row.get("nickname", String.class);
        simpleUser.gender = row.get("gender", Integer.class);
        Boolean activated = row.get("is_enabled", Boolean.class);
        if (activated == null) {
            activated = true;
        }
        simpleUser.activated = activated;
        simpleUser.password = row.get("password", String.class);
        return simpleUser;
    }


    public static class Mapper implements BiFunction<Row, RowMetadata, SimpleUser> {
        @Override
        public SimpleUser apply(Row row, RowMetadata rowMetadata) {
            return convert(row, rowMetadata);
        }
    }
}
