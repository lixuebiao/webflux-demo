package com.weike.bsp.security.authentication.captcha

/**
 * @author tiger
 * -- create at 2019-05-30 17:01
 */
interface CaptchaDetailsService {
    /**
     * 根据用户名加载验证码
     *
     * @param username 用户名
     */
    fun loadCaptchaByUsername(username: String?): Any?
}
