package com.weike.bsp

import dev.miku.r2dbc.mysql.MySqlConnectionConfiguration
import dev.miku.r2dbc.mysql.MySqlConnectionFactory
import io.r2dbc.pool.ConnectionPool
import io.r2dbc.pool.ConnectionPoolConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId

@Configuration
class PrimaryDataSourceConfig {

//    @Bean
//    fun config(): MySqlConnectionConfiguration {
//       return MySqlConnectionConfiguration.builder()
//            .host("127.0.0.1")
//           .port(3306)
//           .username("root")
//           .password("12345678")
//           .database("weike-api")
//           .serverZoneId(ZoneId.from(LocalDate.now()))
//           .connectTimeout(Duration.ofSeconds(3))
//           .useServerPrepareStatement()
//            .build()
//    }

//    @Bean
//    fun connectionFactory() : MySqlConnectionFactory {
//        val config = MySqlConnectionConfiguration.builder()
//            .host("127.0.0.1")
//            .port(3306)
//            .username("root")
//            .password("12345678")
//            .database("weike-api")
//            .serverZoneId(ZoneId.from(LocalDate.now()))
//            .connectTimeout(Duration.ofSeconds(3))
//            .useServerPrepareStatement()
//            .build()
//         MySqlConnectionFactory.from(config)
//    }

    @Bean
    fun connectionPool(): ConnectionPool {
        val config = MySqlConnectionConfiguration.builder()
            .host("127.0.0.1")
            .port(3306)
            .username("root")
            .password("12345678")
            .database("weike-project")
            .serverZoneId(ZoneId.of("Asia/Shanghai"))
            .connectTimeout(Duration.ofSeconds(3))
            .useServerPrepareStatement()
            .build()

        val connectionFactory = MySqlConnectionFactory.from(config)

        val cpc = ConnectionPoolConfiguration.builder(connectionFactory)
            .maxIdleTime(Duration.ofMillis(1000))
            .maxSize(20)
            .build()
        return ConnectionPool(cpc)
    }

    // Mono<Connection> connectionMono = Mono.from(connectionFactory.create());
}