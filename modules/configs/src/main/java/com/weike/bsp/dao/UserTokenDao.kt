package com.weike.bsp.dao

import com.weike.bsp.model.UserToken
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
class UserTokenDao(
    private val databaseClient: DatabaseClient,
    private val template: R2dbcEntityTemplate
) {

    fun findAll(): Flux<UserToken> {
        return databaseClient.sql("select * from pub_access_token at left join pub_user pu on at.user_id = pu.user_id")
            .map { row, m ->  UserToken.convert(row, m) }
            .all()
    }
}