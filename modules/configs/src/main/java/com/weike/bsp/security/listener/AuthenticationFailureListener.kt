package com.weike.bsp.security.listener

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent
import org.springframework.stereotype.Component

/**
 * (spring的事件监听)
 *
 *
 * 认证失败时处理
 */
@Component
class AuthenticationFailureListener @Autowired constructor(
) : ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    override fun onApplicationEvent(e: AuthenticationFailureBadCredentialsEvent) {
        val username = e.authentication.name
        logger.info("当前登录失败的用户账号为: {}", username)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AuthenticationFailureListener::class.java)
    }

}
