package com.weike.bsp.common.util

object Constant {
    /**
     * 常用的日期格式 yyyy-MM-dd HH:mm:ss
     */
    const val DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
    const val DATE_FORMAT_DEFAULT = "yyyy-MM-dd"
    const val DATE_FORMAT_YYYYMMDD = "yyyyMMdd"
    const val DATE_FORMAT_YYYY_MM_DD = "yyyy/MM/dd"
    const val DEFAULT_DATE_FORMAT = DATE_FORMAT_DEFAULT
    const val TIME_FORMAT_DEFAULT = "HH:mm:ss"
    const val TIME_FORMAT_HH_MM = "HH:mm"
    const val TIME_FORMAT_MM_SS = "mm:ss"
    const val DEFAULT_TIME_FORMAT = TIME_FORMAT_DEFAULT
}