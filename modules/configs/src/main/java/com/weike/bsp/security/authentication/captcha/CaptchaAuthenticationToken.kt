package com.weike.bsp.security.authentication.captcha

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority

/**
 * @author tiger
 * -- create at 2019-05-30 15:53
 *
 *
 * 登录的时候需要验证验证码
 */
class CaptchaAuthenticationToken @JvmOverloads constructor(
        /**
         * 验证码
         */
        var captureCode: Any?,
        principal: Any?,
        credentials: Any?,
        authorities: Collection<GrantedAuthority> = emptySet()
) : UsernamePasswordAuthenticationToken(principal, credentials, authorities) {

    companion object {
        private const val serialVersionUID = -3379367862067477567L

        fun from(captcha: String, u: UsernamePasswordAuthenticationToken): CaptchaAuthenticationToken {
            val c = CaptchaAuthenticationToken(captcha, u.principal, u.credentials, u.authorities)
            c.details = u.details
            c.isAuthenticated = u.isAuthenticated
            return c
        }
    }
}
