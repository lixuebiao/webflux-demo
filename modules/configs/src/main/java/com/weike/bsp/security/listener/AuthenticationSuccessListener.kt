package com.weike.bsp.security.listener

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.stereotype.Component

/**
 * @author tiger
 * (spring的事件监听)
 *
 * 认证成功时处理
 */
@Component
class AuthenticationSuccessListener : ApplicationListener<AuthenticationSuccessEvent> {

    override fun onApplicationEvent(event: AuthenticationSuccessEvent) {
        val username = event.authentication.name
        logger.info("当前登录成功的用户账号为: {}", username)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AuthenticationSuccessListener::class.java)
    }

}
