package com.weike.bsp.security.authentication.captcha

import org.springframework.security.core.AuthenticationException

/**
 * 验证码错误异常
 *
 * @author lxb
 */
class CaptchaException @JvmOverloads constructor(msg: String?, t: Throwable? = null) : AuthenticationException(msg, t) {

    companion object {
        /**
         *
         */
        private const val serialVersionUID = 1L
    }
}
