package com.weike.bsp.user

import com.weike.bsp.WeikeApiApplication
import com.weike.bsp.dao.AccessTokenRepo
import com.weike.bsp.dao.UserTokenDao
import com.weike.bsp.model.AccessToken
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDateTime

@SpringBootTest(classes = [WeikeApiApplication::class])
class UserTokenTest {

    @Autowired
    private lateinit var userTokenDao: UserTokenDao

    @Test
    fun test() {
        val i = userTokenDao.findAll().buffer().blockFirst()
//        val i = accessTokenRepo.deleteByToken("test4").block()
        println(i)
    }

}
