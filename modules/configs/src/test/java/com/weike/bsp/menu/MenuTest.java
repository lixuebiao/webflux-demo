package com.weike.bsp.menu;


import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuTest {

    @Test
    public void test() {
        String path = "1/2/3/4/5";

        Pattern p = Pattern.compile("/");

        Matcher m = p.matcher(path);

        while (m.find()) {
            System.out.println(m.group() + "  " + m.start() + "  " + path.substring(0,m.start()));
        }

    }

    @Test
    public void testPath() {
        String path = "1/2/3/4/15";
//        Pattern p = Pattern.compile("/");
//        Matcher m = p.matcher(path);
//        m.group(m.groupCount());
//        System.out.println(m.groupCount());

        int i = path.lastIndexOf("/");
        System.out.println(path.substring(i + 1));
    }
}
