package com.weike.bsp.user

import com.weike.bsp.WeikeApiApplication
import com.weike.bsp.dao.AccessTokenRepo
import com.weike.bsp.model.AccessToken
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDateTime

@SpringBootTest(classes = [WeikeApiApplication::class])
class UserTest {

    @Autowired
    private lateinit var accessTokenRepo: AccessTokenRepo

    @Test
    fun testDel() {
        val i = accessTokenRepo.deleteInIds(listOf(1612426306756, 1612426589938, 1612426853942, 1612426910431)).block()
//        val i = accessTokenRepo.deleteByToken("test4").block()
        println(i)
    }

    @Test
    fun testInsert() {
        val at = AccessToken().apply {
            token = "test7"
            userId = 7L
            username = "admin7"
            createdAt = LocalDateTime.now()
            id = System.currentTimeMillis()
            expiredAt = LocalDateTime.now().plusDays(7)
        }
        val i = accessTokenRepo.insert2(at)
            .block()

        println(i)

//        Thread.sleep(25000)
    }

    @Test
    fun test() {

        accessTokenRepo.findAll()
            .subscribe {
                println(it)
            }

        Thread.sleep(30000)
//        val u = UserQueryCriteria()
//        u.birthday = "2012-12-12~2020-12-12"
//        println(u)
//        println(u.startBirthday)
//        println(u.endBirthday)
    }
//
//    @Test
//    fun testJwt() {
//        val alg = SignatureAlgorithm.HS256
//        val key = Keys.secretKeyFor(alg) //.getEncoded();
////        val key: Key = Keys.secretKeyFor(SignatureAlgorithm.HS256)
////        println(key)
//
//        val jws = Jwts.builder().setSubject("Joe").signWith(key).compact()
//        println(jws)
//
//        val s = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jws) // .body.subject
//        println(s)
//    }
//
//    @Test
//    fun testJwt2() {
//        // javax.crypto.spec.SecretKeySpec@5880753
//        val key: Key = Keys.secretKeyFor(SignatureAlgorithm.HS256)
////        println(key)
//        val jws = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKb2UifQ.i6PwRa8j0-4x968UgqhlCNnnHyLDKc-NiXFsQLlBNXo"
////        Jwts.claims().
//        val s = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jws).body.subject
//
//        println(s)
//    }
}
