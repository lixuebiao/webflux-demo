//import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
//    kotlin("jvm") //version "1.3.31"
    `kotlin-dsl`
}

//group = WeikePress.group
//version = "1.0-SNAPSHOT"

repositories {
    maven(url = "https://maven.aliyun.com/nexus/content/groups/public/")
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
//    runtimeOnly(project(":baseBuildSrc"))

//    subprojects.forEach {
//        runtimeOnly(it)
//    }

}

//
//tasks.withType<KotlinCompile> {
//    kotlinOptions.jvmTarget = Weike.jvmTarget
//}