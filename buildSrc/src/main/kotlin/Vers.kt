import org.gradle.api.JavaVersion

object Weike {
    const val version = "2.4.2"
    const val group = "com.lxb.weike.demo"

    val javaVersion = JavaVersion.VERSION_1_8
    const val jvmTarget = "1.8"

    const val model = "com.lxb.weike:weike-model:2.5.1" //":weike-model"
    const val commons = "com.lxb.weike:weike-common:2.5.4" //":weike-commons"

    object Press {
        const val version = "5.5.0"
        const val group = "com.weike"
        const val services = ":modules:services"
        const val services_version = "1.0.1"
        const val jwt = ":module-extension:jwt"
    }
}

object Spring {

    object Framework {
        // https://mvnrepository.com/artifact/org.springframework/spring-context
        const val version = "5.3.9"
    }

    object Boot {
        // https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web
        const val version = "2.5.3"
        const val plugin = "org.springframework.boot"

        // https://mvnrepository.com/artifact/io.spring.gradle/dependency-management-plugin
        const val spring_dependency_management_version = "1.0.11.RELEASE"
        const val spring_dependency_management_plugin = "io.spring.dependency-management"
    }

    object Security {
        // https://mvnrepository.com/artifact/org.springframework.security/spring-security-core
        const val version = "5.5.1"
    }

    object Data {
        private const val groupId = "org.springframework.data"

        // implementation("org.springframework.boot:spring-boot-starter-data-redis-reactive")

        // https://mvnrepository.com/artifact/org.springframework.data/spring-data-redis
        const val redis = "$groupId:spring-data-redis:2.5.0"

        // https://mvnrepository.com/artifact/redis.clients/jedis
        const val jedis = "redis.clients:jedis:3.6.0"

        // https://mvnrepository.com/artifact/org.springframework.data/spring-data-elasticsearch
        const val elasticsearch = "$groupId:spring-data-elasticsearch:4.2.0"
    }

//    object Integration {
//        // https://mvnrepository.com/artifact/org.springframework.integration/spring-integration-mqtt
//        const val mqtt = "org.springframework.integration:spring-integration-mqtt:5.4.6"
//    }
}

object Apache {
    object Commons {
        // https://mvnrepository.com/artifact/org.apache.commons/commons-text
        const val text = "org.apache.commons:commons-text:1.9"
        const val pool = "org.apache.commons:commons-pool2:2.8.0"
    }

//    // https://mvnrepository.com/artifact/org.apache.poi/poi
//    object Poi {
//        private const val version = "4.1.2"
//
//        private const val groupId = "org.apache.poi"
//
//        const val poi = "$groupId:poi:$version"
//        const val poi_ooxml = "$groupId:poi-ooxml:$version"
//    }

    object ShardingSphere {
        private const val version = "4.1.1"

        // https://mvnrepository.com/artifact/org.apache.shardingsphere/sharding-jdbc-core
        const val sharding_jdbc = "org.apache.shardingsphere:sharding-jdbc-core:$version"

        const val sharding_jdbc_starter = "org.apache.shardingsphere:sharding-jdbc-spring-boot-starter:$version"

        const val sharding_jdbc_namespace = "org.apache.shardingsphere:sharding-jdbc-spring-namespace:$version"
    }
}

object Tools {
    // commons-lang3 的替代者
    // https://mvnrepository.com/artifact/cn.hutool/hutool-all
    const val hutool = "cn.hutool:hutool-all:5.7.5"

    // https://mvnrepository.com/artifact/com.github.liaochong/myexcel
    const val myexcel = "com.github.liaochong:myexcel:3.11.4"

    // https://mvnrepository.com/artifact/com.drewnoakes/metadata-extractor
    const val metadata_extractor = "com.drewnoakes:metadata-extractor:2.15.0"

    const val lombok_version = "1.18.20"
    // https://mvnrepository.com/artifact/org.projectlombok/lombok
    const val lombok = "org.projectlombok:lombok:${lombok_version}"

    // https://mvnrepository.com/artifact/org.jsoup/jsoup
    const val jsonp = "org.jsoup:jsoup:1.13.1"

    // https://mvnrepository.com/artifact/com.github.ben-manes.caffeine/caffeine
    const val caffeine = "com.github.ben-manes.caffeine:caffeine:2.9.1"
}

object Junit {
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
    const val junit5_version = "5.7.1"
    const val junit5_jupiter_api = "org.junit.jupiter:junit-jupiter-api:$junit5_version"
    const val junit5_jupiter = "org.junit.jupiter:junit-jupiter:$junit5_version"
    const val junit5_jupiter_engine = "org.junit.jupiter:junit-jupiter-engine:$junit5_version"
}

object Logging {
    object Log4j2 {
        private const val version = "2.14.1"
        private const val groupId = "org.apache.logging.log4j"

        // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
        const val core = "$groupId:log4j-core:$version"
        const val api = "$groupId:log4j-api:$version"
        const val web = "$groupId:log4j-web:$version"
        const val slf4j_impl = "$groupId:log4j-slf4j-impl:$version"
    }

    object Slf4j {
        private const val version = "1.7.30"
        private const val groupId = "org.slf4j"

        // https://mvnrepository.com/artifact/org.slf4j/slf4j-api
        const val api = "$groupId:slf4j-api:$version"
        const val jcl_over_slf4j = "$groupId:jcl-over-slf4j:$version"
    }
}

object Json {

    object Jackson {
        private const val version = "2.12.4"
        private const val groupId = "com.fasterxml.jackson.core"

        const val core = "$groupId:jackson-core:$version"
        const val annotations = "$groupId:jackson-annotations:$version"
        const val databind = "$groupId:jackson-databind:$version"

        // https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-kotlin
        const val module_kotlin = "com.fasterxml.jackson.module:jackson-module-kotlin:$version"

        // https://mvnrepository.com/artifact/com.fasterxml.jackson.datatype/jackson-datatype-jsr310
        const val jsr310 = "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$version"
    }

//    // https://mvnrepository.com/artifact/com.alibaba/fastjson
//    const val fastjson = "com.alibaba:fastjson:1.2.72"
//
    // https://mvnrepository.com/artifact/com.google.code.gson
    const val gson = "com.google.code.gson:gson:2.8.7"

    /**
     * JSON Web Token
     */
    object WebToken {
        private const val version = "0.11.2"
        private const val groupId = "io.jsonwebtoken"

        // https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt-api
        const val jjwt_api = "$groupId:jjwt-api:$version"
        const val jjwt_impl = "$groupId:jjwt-impl:$version"
        const val jackson = "$groupId:jjwt-jackson:$version"
    }
}

object Mybatis {
    // https://mvnrepository.com/artifact/org.mybatis/mybatis
    const val dependency = "org.mybatis:mybatis:3.5.7"
    // https://mvnrepository.com/artifact/org.mybatis/mybatis-spring
    const val spring = "org.mybatis:mybatis-spring:2.0.6"
    // https://mvnrepository.com/artifact/com.github.pagehelper/pagehelper
    const val pagehelper = "com.github.pagehelper:pagehelper:5.2.1"
    // https://mvnrepository.com/artifact/org.mybatis.spring.boot/mybatis-spring-boot-starter
    const val mybatis_boot = "org.mybatis.spring.boot:mybatis-spring-boot-starter:2.2.0"
    // https://mvnrepository.com/artifact/com.github.pagehelper/pagehelper-spring-boot-starter
    const val pagehelper_boot = "com.github.pagehelper:pagehelper-spring-boot-starter:1.3.1"
}

//object Db {
//    // https://mvnrepository.com/artifact/mysql/mysql-connector-java
//    const val mysql = "mysql:mysql-connector-java:8.0.20"
//    // https://mvnrepository.com/artifact/com.alibaba/druid
//    const val druid = "com.alibaba:druid:1.1.23"
//    const val druid_boot = "com.alibaba:druid-spring-boot-starter:1.1.23"
//}

object Jakarta {
    const val validation_api = "jakarta.validation:jakarta.validation-api:2.0.2"
    const val servlet_api = "jakarta.servlet:jakarta.servlet-api:4.0.3"
    const val servlet_jsp_api = "jakarta.servlet.jsp:jakarta.servlet.jsp-api:2.3.6"
    const val servlet_jsp_api_jstl = "jakarta.servlet.jsp.jstl:jakarta.servlet.jsp.jstl-api:1.2.7"
    const val mail = "com.sun.mail:jakarta.mail:1.6.5"
}

object Dependencies {
    // https://mvnrepository.com/artifact/org.hibernate.validator/hibernate-validator
    const val validator = "org.hibernate.validator:hibernate-validator:6.1.6.Final"

    // https://mvnrepository.com/artifact/org.aspectj/aspectjweaver
    const val aspectjweaver = "org.aspectj:aspectjweaver:1.9.6"

    // https://mvnrepository.com/artifact/org.jsoup/jsoup
    const val jsonp = "org.jsoup:jsoup:1.13.1"

    // https://mvnrepository.com/artifact/com.github.binarywang/weixin-java-mp
    // 微信公众号 sdk
    const val wx_mp = "com.github.binarywang:weixin-java-mp:4.0.0"
    // 微信小程序 sdk
    const val wx_miniapp = "com.github.binarywang:weixin-java-miniapp:4.0.0"
    // 微信支付 sdk
    const val wx_pay = "com.github.binarywang:weixin-java-pay:4.0.0"
}

// https://mvnrepository.com/artifact/com.google.zxing/core
object Zxing {
    private const val version = "3.4.1"
    const val core = "com.google.zxing:core:$version"
    const val javase = "com.google.zxing:javase:$version"
}

//object Templates {
//    const val freemarker = "org.freemarker:freemarker:2.3.30"
//    // https://mvnrepository.com/artifact/org.thymeleaf/thymeleaf
//    object Thymeleaf {
//        private const val version = "3.0.11.RELEASE"
//        private const val groupId = "org.thymeleaf"
//
//        const val dependency = "$groupId:thymeleaf:$version"
//        const val spring5 = "$groupId:thymeleaf-spring5:$version"
//
//        // https://mvnrepository.com/artifact/org.thymeleaf.extras/thymeleaf-extras-springsecurity5
//        const val security5 = "$groupId.extras:thymeleaf-extras-springsecurity5:3.0.4.RELEASE"
//    }
//}

// https://mvnrepository.com/artifact/org.quartz-scheduler/quartz
object Quartz {
    private const val version = "2.3.2"
    private const val groupId = "org.quartz-scheduler"

    const val dependency = "$groupId:quartz:$version"
    const val jobs = "$groupId:quartz-jobs:$version"
}

object OpenApi {
    private const val springdoc_version = "1.5.9"

    // https://springdoc.org/
    // https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-ui
    const val springdoc = "org.springdoc:springdoc-openapi-ui:${springdoc_version}"

    // security
    const val springdoc_security = "org.springdoc:springdoc-openapi-security:${springdoc_version}"

    // kotlin 支持
    const val springdoc_kt = "org.springdoc:springdoc-openapi-kotlin:${springdoc_version}"

    // https://mvnrepository.com/artifact/io.springfox/springfox-boot-starter
    const val springfox = "io.springfox:springfox-boot-starter:3.0.0"
}

object Kotlin {
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib-common
    const val version = "1.5.21"
    const val plugin = "kotlin"
    const val jvm = "jvm"
}

object KotlinX {
    object Serialization {
        const val version = "1.2.2"
        const val plugin = "kotlinx-serialization"
        // "org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0"

        // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-core
        const val core = "org.jetbrains.kotlinx:kotlinx-serialization-core:${version}"
        const val json = "org.jetbrains.kotlinx:kotlinx-serialization-json:${version}"
    }

    object Coroutines {
        const val version = "1.5.1"
        // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-android
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${version}"
        // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core-jvm
        const val jvm = "org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:${version}"
    }
}
